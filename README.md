The point of this repo is to standup and endpoint where I can input an etf symbol, a start date and an end date and get a list of all the copmonents of that ETF that outperformed the aggregate ETF. This will eventually feed a front end I want to make in react to display information about these outperformers to make investment decisions

lambda event structure: 
event = {
    "ticker": {valid_etf_ticker}
    "start": "month/day/year"
    "end": "month/day/year"
}
I don't have robust error handling so if you're going to hit this endpoint it needs to be structured appropriately or it wont work

The lambda saves the holdings of an etf to a dynamodb table indexed by ticker, this is so it doesnt have to make the calls to scrape for holdings eveyrtime we make a call for a ticker, the holdings dont change dramatically often

because dynamodb cant take floats we convert the price change from % to %*100000 so if you want to convert back to % you need to divide by 100000 (or /1000 if you want to percentage and not the true number value for calculations)

Another restriction is the 30 second api gateway timeout, this is not configurable and I cannot guarantee processing takes fewer than 30 seonds so I made another function to query the db for data thats already been processed