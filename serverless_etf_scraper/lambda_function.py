import json
import sys
import urllib.request
import datetime
import boto3
import time
import yfinance as yf
import pandas as pd
from multiprocessing import Process, Manager
from bs4 import BeautifulSoup
from pandas_datareader import data as pdr
yf.pdr_override()

DYNAMO_DB_HOLDINGS_TABLE_NAME='ETFHoldings'
ETF_TICKER_LIST = ['BETZ','ONLN','QQQ','SPY','DIA','IWM','IWP','ITOT', 'IWV', 'SCHB', 'FNDB', 'VT', 'VTI', 'VXUS', 'VTHR', 'DIA', 'RSP', 'IOO', 'IVV', 'SPY', ' SHE', 'VOO', 'IWM', 'OEF', 'CVY', 'RPG', 'RPV', 'IWB', 'IWF', 'IWD', 'IVV', 'IVW', 'IVE', 'PKW', 'PRF', 'SPLV', 'SCHX', 'SCHG', 'SCHV', 'SCHD', 'FNDX', 'SDY', 'VOO', 'VOOG', 'VOOV', 'VV', 'VUG', 'VTV', 'MGC', 'MGK', 'MGV', 'VONE', 'VONG', 'VONV', 'VIG', 'VYM', 'DTN', 'DLN', 'MDY', 'DVY', 'IWR', 'IWP', 'IWS', 'IJH', 'IJK', 'IJJ', 'PDP', 'SCHM', 'IVOO', 'IVOG', 'IVOV', 'VO', 'VOT', 'VOE', 'VXF', 'DON', 'IWC', 'IWM', 'IWO', 'IWN', 'IJR', 'IJT', 'IJS', 'SCHA', 'FNDA', 'VIOO', 'VIOG', 'VIOV', 'VB', 'VBK', 'VBR', 'VTWO', 'VTWG', 'VTWV', 'EEB', 'ECON', 'IDV', 'BKF', 'EFA', 'EFG', 'EFV', 'SCZ', 'EEM', 'PID', 'SCHC', 'SCHE', 'SCHF', 'FNDF', 'FNDC', 'FNDE', 'DWX', 'VEA', 'VWO', 'VXUS', 'VEU', 'VSS', 'DEM', 'DGS', 'EZU', 'EPP', 'IEV', 'ILF', 'FEZ', 'VGK', 'VPL', 'HEDJ', 'DFE', 'AND', 'GXF', 'EWA', 'EWC', 'EWG', 'EIS', 'EWI', 'EWJ', 'EWD', 'EWL', 'EWP', 'EWU', 'DXJ', 'NORW', 'INDF', 'EWZ', 'FXI', 'EWH', 'EWW', 'EPHE', 'RSX', 'EWS', 'EWM', 'EWY', 'EWT', 'EPI', 'ARGT', 'BRAF', 'BRAQ', 'BRAZ', 'GXG', 'XLY', 'IYC', 'ITB', 'XHB', 'VCR', 'XLP', 'IYK', 'VDC', 'AMLP', 'XLE', 'IYE', 'IGE', 'OIH', 'XOP', 'VDE', 'ESPO', 'XLF', 'IYF', 'KBE', 'KRE', 'VFH', 'FXH', 'FBT', 'XLV', 'IYH', 'PJP', 'XBI', 'VHT', 'XLI', 'IYJ', 'VIS', 'XLB', 'IYM', 'GDX', 'GDXJ', 'VAW', 'FDN', 'XLK', 'IYW', 'IGV', 'VGT', 'IYZ', 'VOX', 'XLU', 'IDU', 'VPU', 'IPD', 'RXI', 'IPS', 'KXI', 'IPW', 'IXC', 'IPF', 'IXG', 'IRY', 'IXJ', 'IPN', 'EXI', 'GUNR', 'IRV', 'MXI', 'IPK', 'IXN', 'IST', 'IXP', 'IPU', 'JXI', 'HYLD', 'TDTT', 'CSJ', 'IEI', 'AGG', 'SHY', 'TIP', 'HYG', 'LQD', 'IEF', 'TLT', 'FLOT', 'CIU', 'GVI', 'EMB', 'MBB', 'MUB', 'SHV', 'HYD', 'HYS', 'STPZ', 'MINT', 'BOND', 'PCY', 'BKLN', 'SCHZ', 'SCHP', 'SCHO', 'SCHR', 'JNK', 'BIL', 'JNK', 'SCPB', 'BWX', 'SJNK', 'TFI', 'SHM', 'EDV', 'BIV', 'VCIT', 'VGIT', 'BLV', 'VCLT', 'VGLT', 'VMBS', 'BSV', 'VCSH', 'VGSH', 'BND', 'RJI', 'DJP', 'GSG', 'DBC', 'RJA', 'JJA', 'DBA', 'RJN', 'OIL', 'GAZ', 'UNG', 'USO', 'RJZ', 'JJM', 'JJC', 'DBB', 'SGOL', 'IAU', 'GLD', 'SIVR', 'SLV', 'PALL', 'PPLT', 'ICF', 'IFAS', 'IFEU', 'IYR', 'REM', 'SCHH', 'RWO', 'RWX', 'RWR', 'WREI', 'VNQ', 'VNQI', 'HDGE', 'HDGI', 'DOG', 'SH', 'MYY', 'SBB', 'PSQ', 'RWM', 'EFZ', 'FBGX', 'FLGE', 'MIDU', 'SPUU', 'SPXL', 'ERX', 'FAS', 'BGU', 'TNA', 'DDM', 'QLD', 'UWM', 'SSO', 'UPRO', 'SDS', 'SPXU', 'TZA', 'QID', 'SKF', 'TWM', 'DXD', 'SRS', 'MZZ', 'DUG', 'BGZ', 'ERY', 'FAZ', 'AADR', 'ACCU', 'DBIZ', 'EPRO', 'FWDB', 'FWDD', 'FWDI', 'GEUR', 'GGBP', 'GIVE', 'GLDE', 'GYEN', 'GTAA', 'HDGE', 'HDGI', 'HOLD', 'HYLD', 'MATH', 'MINC', 'QEH', 'TTFS', 'VEGA', 'YPRO', 'RIGS', 'ARKG', 'ARKQ', 'ARKW', 'ARKK', 'SYLD', 'GMMB', 'GMTB', 'GVT', 'RPX', 'RWG', 'EMLP', 'FMB', 'FMF', 'FPE', 'FTGS', 'FTHI', 'FTLB', 'FTSL', 'HYLS', 'RAVI', 'FTSD', 'GSY', 'HECO', 'HUSE', 'ICSH', 'IEIL', 'IEIS', 'IELG', 'IESM', 'NEAR', 'BABZ', 'BOND', 'DI', 'FORX', 'ILB', 'LDUR', 'MINT', 'MUNI', 'SMMU', 'CHNA', 'LALT', 'PHDG', 'PSR', 'ONEF', 'GAL', 'INKM', 'RLY', 'SYE', 'SYG', 'SYV', 'SRLN', 'ULST', 'ALD', 'AUNZ', 'BZF', 'CCX', 'CEW', 'CRDT', 'CYB', 'ELD', 'EMCB', 'EU', 'ICB', 'RRF', 'USDU', 'WDTI']

def isNan(number):
    return number != number

def display_data(ticker, start, end):
    stock = yf.download(ticker, start=start, end=end, progress=False)
    stock['Close'].plot(subplots=['Volume'], figsize=(30, 17))

def get_from_db(key_name, key_value, tableName, dynamodb=None):
    if not dynamodb:
        dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table(tableName)
    response = table.get_item(
        Key={
            key_name: key_value
        }
    )
    return response

def put_in_db(payload, tableName, dynamodb=None):
    if not dynamodb:
        dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table(tableName)
    response = table.put_item(
        Item=payload
    )

def get_proxy():
    url = "https://free-proxy-list.net"
    req = urllib.request.Request(url)
    req.add_header("Cookie","__cfduid=d58943e5820ec576ff6ccbb3466390d8f1608497301; _ga=GA1.2.1537521014.1608497303; _gid=GA1.2.1472302264.1608497303; _gat=1; MicrosoftApplicationsTelemetryDeviceId=58f1e257-82a1-923c-5625-53d029c23d14; MicrosoftApplicationsTelemetryFirstLaunchTime=1608497303167")
    req.add_header('User-Agent',
                'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:83.0) Gecko/20100101 Firefox/83.0'
                )
    with urllib.request.urlopen(req) as f:
        data = f.read().decode('utf-8')
        soup = BeautifulSoup(data, 'html.parser')
        proxy_table = soup.find("tbody")
        proxy_information = proxy_table.find_all("td")
        proxy = (str(proxy_information).split(",")[0:2])
        [ip,port] = proxy
        ip = ip[ip.index(">")+1:]
        ip = ip[:ip.index("<")]
        port = port[port.index(">")+1:]
        port = port[:port.index("<")]
        return ip + ":" + port


def parse_date(date):
    [month, day, year] = date.split('/')
    return datetime.datetime(int(year),int(month),int(day))

def parse_get_holdings_input(event):
    ticker = event['body']['ticker']
    start = parse_date(event['body']['start'])
    end = parse_date(event['body']['end'])
    return[ticker, start, end]

def parse_get_outperformers_input(event):
    holdings = event['body']['holdings']
    start = parse_date(event['body']['start'])
    end = parse_date(event['body']['end'])
    threshold = event['body']['threshold']
    return[holdings,start,end,threshold]

#Contract: this takes an event that looks like
# event = {
#     body: {
#         holdings: HOLDINGS_LIST,
#         start: DATE MM/DD/YYYY,
#         end: DATE MM/DD/YYYY,
#         threshold: THRESHOLD
#     }
# }
# same rules as the other endpoint where the body is added in the api gateway
# where the holdings, and threshold are returned from the get_holdings endpoint and the start and end date are the same as the ones that
# where input into the get_holdings endpoint
# it returns a json that looks like this
# {
#     'statusCode': 200,
#     'headers': {
#         'Access-Control-Allow-Origin': '*'
#     },
#     'body': [["GS", 0.032316442605, 2308552], [ticker, performance, volume]]
# }
def get_outperformers(event,context):
    start_time = time.time()
    [holdings, start, end, threshold] = parse_get_outperformers_input(event)
    multi_process_outperformers = Manager().list()
    processess = []
    batch_tickers = []
    i = 0
    #the goal is to call another lambda to take advantage of all the concurrent lambdas we have available. I want to ru nas many processess on a lmabda as I can without any being stuck in a queue
    # so this breaks it up into 16 and sends it off to its own lambda for more paralellization
    for ticker in holdings:
        batch_tickers.append(ticker)
        if(i == 16):
            proc = Process(target=get_outperformers_async, args=(batch_tickers, event['body']['start'], event['body']['end'], threshold, multi_process_outperformers))
            processess.append(proc)
            proc.start()
            i = 0
            batch_tickers = []
        else: 
            i = i + 1
    if batch_tickers:
        proc = Process(target=get_outperformers_async, args=(batch_tickers, event['body']['start'], event['body']['end'], threshold, multi_process_outperformers))
        processess.append(proc)
        proc.start()
    for proc in processess:
        proc.join()
    end_time = time.time()
    print("get_outperformers_execution_time: ", end_time-start_time)
    #print("mpop: ", multi_process_outperformers)
    return {
        'statusCode': 200,
        'headers': {
            'Access-Control-Allow-Origin': '*'
        },
        'body': json.dumps(list(multi_process_outperformers))
    }

def get_outperformers_async(holdings, start, end, threshold, multi_process_outperformers):
    body = {
        "holdings": holdings,
        "start": start,
        "end": end,
        "threshold": threshold
    }
    url = "https://mx72grwq5j.execute-api.us-east-1.amazonaws.com/dev/getOutperformersBatch"
    req = urllib.request.Request(url, data=urllib.parse.urlencode(body).encode())
    data = urllib.request.urlopen(req)
    response_body = json.loads(data.read())
    response_body = json.loads(response_body['body'])
    for body in response_body:
        multi_process_outperformers.append(body)
    return

#This is the second lambda that we will be working with, it expcets an array of 16 tickers (could be less) and it should start a process for each one and find the ourperformers for it and report back
def get_outperformers_batch_lambda_handler(event, context):
    start_time = time.time()
    #print("event: ", event)
    holdings = event["body"]["holdings"][1:-1].split(",")
    holdings = [ ticker.replace("'","") for ticker in holdings ]
    start = parse_date(event["body"]["start"])
    end = parse_date(event["body"]["end"])
    threshold = event["body"]["threshold"]
    multi_process_outperformers = Manager().list()
    processess = []
    for ticker in holdings:
        proc = Process(target=outperformer_check, args=(ticker, start, end, float(threshold), multi_process_outperformers))
        processess.append(proc)
        proc.start()
    for proc in processess:
        proc.join()
    end_time = time.time()
    print("get_outperformers_batch_lambda_handler: ", end_time-start_time, " size: ", len(holdings))
    #print("outperformers: ", list(multi_process_outperformers))
    return {
        'statusCode': 200,
        'headers': {
            'Access-Control-Allow-Origin': '*'
        },
        'body': json.dumps([{"ticker": x[0].strip(), "performance": x[1], "average_volume": x[2]} for x in multi_process_outperformers])
    }

#this is the lowest level function call that just gets the data for one ticker and reports back if its higher than a certain threshold
def outperformer_check(ticker, start, end, threshold, outperformers):
    start_time = time.time()
    #ETFDB returns some results in the "other" category like short interest, this may be good to add on the website but for now lets just ignore it
    if(not ticker or ticker == 'Other'):
        return
    try:  
        stock = pdr.get_data_yahoo(ticker, start=start, end=end, progress=False)
    except:
        print("error getting stock information ", ticker, " : ", sys.exc_info())
        return
    if stock.empty: 
        print("error getting stock information ", ticker, " : ", sys.exc_info())
        return
    price_change = (round(float(stock['Close'][-1]), 3) - round(float(stock['Close'][0]), 3)) / round(float(stock['Close'][0]), 3)
    if(price_change < float(threshold)):
        return
    avg_volume = sum(stock['Volume']) / len(stock['Volume'])
    if(isNan(avg_volume) or isNan(price_change)):
        return
    outperformers.append([ticker, price_change, int(avg_volume)])
    end_time = time.time()
    print("outperformer_check: ", end_time-start_time)
    return


#Contract: this takes an event that looks like this
# event = {
#     body: {
#         ticker: TICKER,
#         start: START,
#         end: END
#     }
# }
# However the body is added in the api gateway so when sending a request from the front end it just has to look like this
# {
#     ticker: TICKER,
#     start: DATE MM/DD/YYYY,
#     end: DATE MM/DD/YYYY
# }
# it returns a json that looks like this
# {
#     'statusCode': 200,
#     'headers': {
#         'Access-Control-Allow-Origin': '*',
#         'Access-Control-Allow-Methods': '*',
#         'Access-Control-Allow-Headers': '*'
#     },
#     'body': {
#         'holdings': stocks_in_etf,
#         'performance': performance
#     }
# }
# holdings are a list of tickers to be fed into the get_outperformers endpoint
# performance is a string that can be converted to a float or fed into the next function call
def get_holdings(event,context):
    start_time = time.time()
    if not event or event == "":
        event = {
            'body': {
                'ticker': 'ICLN',
                "start": "12/1/2020",
                "end": "12/5/2020"
            }
        }
    [ticker,start,end] = parse_get_holdings_input(event)
    # if(ticker not in ETF_TICKER_LIST):
    #     return {
    #         'statusCode': 400,
    #         'body': 'ERROR: TICKER NOT AN ETF'
    #     }
    #find etf performance
    performance = "-1.0"
    try:
        etf_performance_information = yf.download(ticker, start, end)
        etf_performance_information = etf_performance_information.dropna()
        performance = (round(float(etf_performance_information['Close'][-1]),3)-round(float(etf_performance_information['Close'][0]),3))/round(float(etf_performance_information['Close'][0]),3)
    except:
        print("error getting etf performance information from yahoo finance")
    #get_holding_information
    print("received performance")
    stocks_in_etf = get_from_db("etf",ticker.upper(),DYNAMO_DB_HOLDINGS_TABLE_NAME)
    db = False
    if("Item" not in stocks_in_etf):
        print('Not in database, scrape')
        stocks_in_etf = scrape_holdings(ticker.upper())
    else:
        print("got from database")
        db = True
        stocks_in_etf = stocks_in_etf["Item"]["holdings"]
    if(not stocks_in_etf):
        return {
            'statusCode': 200,
            'headers': {
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': '*',
                'Access-Control-Allow-Headers': '*'
            },
            'body': 'ERROR RETRIEVING STOCKS IN ETF'
        }
    print("got stocks: ", stocks_in_etf)
    print(type(stocks_in_etf))
    if not db:
        print(type(stocks_in_etf["holdings"]))
        prepare_dict = json.loads(stocks_in_etf["holdings"])["Symbol"]
        stocks_in_etf = [prepare_dict[q] for q in prepare_dict]
        print(stocks_in_etf)
    else: 
        stocks_in_etf = pd.read_json(stocks_in_etf)['Symbol']
    end_time = time.time()
    print("get_holdings_execution_time: ", end_time-start_time)
    return {
        'statusCode': 200,
        'headers': {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': '*',
            'Access-Control-Allow-Headers': '*'
        },
        'body': {
            'holdings': list(stocks_in_etf),
            'performance': performance
        }
    }

def get_etfs():
    url = "https://etfdb.com/export/etfs_details_type_fund_flow.csv?scope={\"by_type\":[\"Etfdb::EtfType\",17,null,false,false]}"
    req = urllib.request.Request(url)
    req.add_header("Cookie","Cookie: __utma=148430563.1199296581.1606791764.1608170913.1614208277.4; __utmz=148430563.1608170913.3.3.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided); _hjid=e880f221-c1ca-42ce-b491-47ab04c899ca; __gads=ID=3f84411d74154e72-2203e847407a0055:T=1606791764:S=ALNI_MZ0SvJskoEJROH4gGtFZwWZisKViQ; etf_iSv=; etf_iSe=2147483647000; etf_iSc=0; dw_data_checked=true; _hjIncludedInSessionSample=1; etf_ced=true; _dummy_session=eyJzZXNzaW9uX2lkIjoiZjI0MTdhZjg3MmFhZjFkYzE5ZDQ5MDBhNmJhMDA0MGMiLCJfY3NyZl90b2tlbiI6Im1hTVkzRnJOMGFZRThhaGdoQ2xEWjRGbUtnL2c5aWs3SU5QTFZmMWVacGs9IiwiX2VtYWlsIjoic29zYXkyMjkyNUBkeGVjaWcuY29tIiwiX2ludmVzdG9yX3R5cGUiOiJJbmRpdmlkdWFsIEludmVzdG9yIn0%3D--14e989dda49405cf6abb3349b40ab9c96b0fb01e; etf_aSv=; etf_aSe=2147483647000; wordpress_logged_in_c5b26572b38876ec065ad6a5f98894a1=sno1wverload%7C1614381141%7C2ea09d54c4670459ad7eba0c9f776cc9")
    with urllib.request.urlopen(req) as f:
        data = pd.read_csv(f)
        data.to_csv('EQUITY_ETFS.csv')

def scrape_holdings(etf):
    proxy = get_proxy()
    try: 
        url = "https://etfdb.com/etf/" + etf.upper() + "/"
        req = urllib.request.Request(url)
        cookie = "Cookie: __utma=148430563.1199296581.1606791764.1608170913.1614208277.4; __utmz=148430563.1608170913.3.3.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided); _hjid=e880f221-c1ca-42ce-b491-47ab04c899ca; __gads=ID=3f84411d74154e72-2203e847407a0055:T=1606791764:S=ALNI_MZ0SvJskoEJROH4gGtFZwWZisKViQ; etf_iSv=; etf_iSe=2147483647000; etf_iSc=0; dw_data_checked=true; _hjIncludedInSessionSample=1; etf_ced=true; _dummy_session=eyJzZXNzaW9uX2lkIjoiZjI0MTdhZjg3MmFhZjFkYzE5ZDQ5MDBhNmJhMDA0MGMiLCJfY3NyZl90b2tlbiI6Im1hTVkzRnJOMGFZRThhaGdoQ2xEWjRGbUtnL2c5aWs3SU5QTFZmMWVacGs9IiwiX2VtYWlsIjoic29zYXkyMjkyNUBkeGVjaWcuY29tIiwiX2ludmVzdG9yX3R5cGUiOiJJbmRpdmlkdWFsIEludmVzdG9yIn0%3D--14e989dda49405cf6abb3349b40ab9c96b0fb01e; etf_aSv=; etf_aSe=2147483647000; wordpress_logged_in_c5b26572b38876ec065ad6a5f98894a1=sno1wverload%7C1614381141%7C2ea09d54c4670459ad7eba0c9f776cc9"
        req.add_header("Cookie","Cookie: __utma=148430563.1199296581.1606791764.1608170913.1614208277.4; __utmz=148430563.1608170913.3.3.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided); _hjid=e880f221-c1ca-42ce-b491-47ab04c899ca; __gads=ID=3f84411d74154e72-2203e847407a0055:T=1606791764:S=ALNI_MZ0SvJskoEJROH4gGtFZwWZisKViQ; etf_iSv=; etf_iSe=2147483647000; etf_iSc=0; dw_data_checked=true; _hjIncludedInSessionSample=1; etf_ced=true; _dummy_session=eyJzZXNzaW9uX2lkIjoiZjI0MTdhZjg3MmFhZjFkYzE5ZDQ5MDBhNmJhMDA0MGMiLCJfY3NyZl90b2tlbiI6Im1hTVkzRnJOMGFZRThhaGdoQ2xEWjRGbUtnL2c5aWs3SU5QTFZmMWVacGs9IiwiX2VtYWlsIjoic29zYXkyMjkyNUBkeGVjaWcuY29tIiwiX2ludmVzdG9yX3R5cGUiOiJJbmRpdmlkdWFsIEludmVzdG9yIn0%3D--14e989dda49405cf6abb3349b40ab9c96b0fb01e; etf_aSv=; etf_aSe=2147483647000; wordpress_logged_in_c5b26572b38876ec065ad6a5f98894a1=sno1wverload%7C1614381141%7C2ea09d54c4670459ad7eba0c9f776cc9")
        #req.set_proxy(proxy, 'http')
        etf_slug = "-1"
        #experiment
        # proxies = {
        #     'http': proxy
        # }
        # results = requests.get(url, proxies=proxies, headers={'Cookie': "Cookie: __utma=148430563.1199296581.1606791764.1608170913.1614208277.4; __utmz=148430563.1608170913.3.3.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided); _hjid=e880f221-c1ca-42ce-b491-47ab04c899ca; __gads=ID=3f84411d74154e72-2203e847407a0055:T=1606791764:S=ALNI_MZ0SvJskoEJROH4gGtFZwWZisKViQ; etf_iSv=; etf_iSe=2147483647000; etf_iSc=0; dw_data_checked=true; _hjIncludedInSessionSample=1; etf_ced=true; _dummy_session=eyJzZXNzaW9uX2lkIjoiZjI0MTdhZjg3MmFhZjFkYzE5ZDQ5MDBhNmJhMDA0MGMiLCJfY3NyZl90b2tlbiI6Im1hTVkzRnJOMGFZRThhaGdoQ2xEWjRGbUtnL2c5aWs3SU5QTFZmMWVacGs9IiwiX2VtYWlsIjoic29zYXkyMjkyNUBkeGVjaWcuY29tIiwiX2ludmVzdG9yX3R5cGUiOiJJbmRpdmlkdWFsIEludmVzdG9yIn0%3D--14e989dda49405cf6abb3349b40ab9c96b0fb01e; etf_aSv=; etf_aSe=2147483647000; wordpress_logged_in_c5b26572b38876ec065ad6a5f98894a1=sno1wverload%7C1614381141%7C2ea09d54c4670459ad7eba0c9f776cc9" })
        # print(results)
        print(url)
        with urllib.request.urlopen(req) as f:
            print("accessed main etf page")
            data = f.read().decode('utf-8')
            soup = BeautifulSoup(data, 'html.parser')
            etf_slug = soup.find('form', {'class': 'button_to'})["action"]
        if(etf_slug == "-1"):
            raise Exception("error getting etf_slug")
        #df = pd.DataFrame(columns=['Name','Weight'])
        new_url = "https://etfdb.com" + etf_slug
        new_req = urllib.request.Request(new_url)
        print("made second url: ", new_url)
        new_req.add_header("Cookie","Cookie: __utma=148430563.1199296581.1606791764.1608170913.1614208277.4; __utmz=148430563.1608170913.3.3.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided); _hjid=e880f221-c1ca-42ce-b491-47ab04c899ca; __gads=ID=3f84411d74154e72-2203e847407a0055:T=1606791764:S=ALNI_MZ0SvJskoEJROH4gGtFZwWZisKViQ; etf_iSv=; etf_iSe=2147483647000; etf_iSc=0; dw_data_checked=true; _hjIncludedInSessionSample=1; etf_ced=true; _dummy_session=eyJzZXNzaW9uX2lkIjoiZjI0MTdhZjg3MmFhZjFkYzE5ZDQ5MDBhNmJhMDA0MGMiLCJfY3NyZl90b2tlbiI6Im1hTVkzRnJOMGFZRThhaGdoQ2xEWjRGbUtnL2c5aWs3SU5QTFZmMWVacGs9IiwiX2VtYWlsIjoic29zYXkyMjkyNUBkeGVjaWcuY29tIiwiX2ludmVzdG9yX3R5cGUiOiJJbmRpdmlkdWFsIEludmVzdG9yIn0%3D--14e989dda49405cf6abb3349b40ab9c96b0fb01e; etf_aSv=; etf_aSe=2147483647000; wordpress_logged_in_c5b26572b38876ec065ad6a5f98894a1=sno1wverload%7C1614381141%7C2ea09d54c4670459ad7eba0c9f776cc9")
        #new_req.set_proxy(proxy, 'http')
        line_number = 0
        with urllib.request.urlopen(new_req) as f:
            print("opened second url")
            data = pd.read_csv(f, skiprows=13)
            payload = {
                "etf": etf.upper(),
                "holdings": data.to_json()
            }
            print("putting in db")
            put_in_db(payload, DYNAMO_DB_HOLDINGS_TABLE_NAME)
            print("put in db")
            return payload
            
    except urllib.error.URLError:
        print("urllib urlError connection refused", etf, sys.exc_info())
        print("You might need to update your key")
        print(proxy)
        return None
    except:
        print("error getting information for ", etf, " : ", sys.exc_info())
        print("You might need to update your key")
        print(proxy)
        return None


def main():
    start_time = time.time()
    ticker = "HERO"
    start = "11/1/2020"
    end = "12/1/2020"
    payload = {
        "body": {
            "ticker": ticker,
            "start": start,
            "end": end
        }
    }
    get_holdings_return_value = get_holdings(payload,"")
    print('get_holdings_return_value: ', get_holdings_return_value)
    holdings = get_holdings_return_value["body"]["holdings"]
    performance = get_holdings_return_value["body"]["performance"]
    new_payload = {
        "body": {
            "holdings": holdings,
            "start": start,
            "end": end,
            "threshold": performance
        }
    }
    get_outperformers_return_value = get_outperformers(new_payload,"")
    print(get_outperformers_return_value)
    end_time = time.time()
    print("total_time: ", end_time-start_time)
if __name__ == "__main__":
    main()
